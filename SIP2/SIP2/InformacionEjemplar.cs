﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIP2
{
    public class InformacionEjemplar
    {
        public string[] titulo { get; set; }
        public string fechaPrestamo { get; set; }
        public string fechaVencimiento { get; set; }
        
    }
}
