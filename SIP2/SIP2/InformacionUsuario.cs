﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIP2
{
    public class InformacionUsuario
    {
        public string institucion { get; set; }
        public string nombreUsuario { get; set; }
        public string correo { get; set; }
        public string telefono { get; set; }
        public string maxNumPrestamos { get; set; }
        public string multa { get; set; }
        public string estado { get; set; }
        public string usuarioValido { get; set; }
        public string contrasenaValida { get; set; }
        public string[] libros_prestados { get; set; }
    }
}
