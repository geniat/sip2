﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIP2
{
    public class InformacionTerminal
    {
        public string institucion { get; set; }
        public string biblioteca { get; set; }
        public string localizacion { get; set; }
        public string estado_servidor { get; set; }
        public string estado_checkout { get; set; }
        public string estado_checkin { get; set; }
        public string estado { get; set; }
    }
}
