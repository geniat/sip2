﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIP2
{
    public class Respuestas
    {
        public InformacionUsuario respuesta_PatronStatusResponse(string sipResponse)
        {
            InformacionUsuario info_usuario = new InformacionUsuario();

            int posInicio = sipResponse.IndexOf("AO");
            int posPipe = sipResponse.IndexOf("|AA");
            string institucion = sipResponse.Substring(posInicio + 2, posPipe - posInicio - 2); //Institucion donde esta la estación de autoprestamo
            sipResponse = sipResponse.Substring(posPipe + 1);

            posInicio = sipResponse.IndexOf("AE");
            posPipe = sipResponse.IndexOf("|BL");
            string nombreUsuario = sipResponse.Substring(posInicio + 2, posPipe - posInicio - 2); //Nombre del Usuario
            sipResponse = sipResponse.Substring(posPipe + 1);

            posInicio = sipResponse.IndexOf("BL");
            posPipe = sipResponse.IndexOf("|CQ");
            string usuarioValido = sipResponse.Substring(posInicio + 2, posPipe - posInicio - 2); //Validacion del Usuario
            sipResponse = sipResponse.Substring(posPipe + 1);

            posInicio = sipResponse.IndexOf("CQ");
            posPipe = sipResponse.IndexOf("|BH");
            string contrasenaValida = sipResponse.Substring(posInicio + 2, posPipe - posInicio - 2); //Validacion de la contraseña
            sipResponse = sipResponse.Substring(posPipe + 1);

            posInicio = sipResponse.IndexOf("BV");
            posPipe = sipResponse.IndexOf("|AF");
            string multa = sipResponse.Substring(posInicio + 2, posPipe - posInicio - 2); //Multa del Usuario
            sipResponse = sipResponse.Substring(posPipe + 1);

            posInicio = sipResponse.IndexOf("AF");
            posPipe = sipResponse.IndexOf("|AG");
            string estado = sipResponse.Substring(posInicio + 2, posPipe - posInicio - 2); //Mensaje opcional que puede traer informacion adicional
            sipResponse = sipResponse.Substring(posPipe + 1);

            info_usuario.institucion = institucion;
            info_usuario.nombreUsuario = nombreUsuario;
            info_usuario.usuarioValido = usuarioValido;
            info_usuario.contrasenaValida = contrasenaValida;
            info_usuario.estado = estado;

            return info_usuario;
        }

        public bool respuesta_CheckOut(string sipResponse)
        {
            string estado = "";
            int inicio = sipResponse.IndexOf("AF");
            int fin = sipResponse.IndexOf("|AY");
            estado = sipResponse.Substring(inicio + 2, fin - inicio - 2);

            if (String.IsNullOrEmpty(estado))
                return true;
            else
                return false;
        }

        public bool respuesta_CheckIn(string sipResponse)
        {
            string estado = "";
            int inicio = sipResponse.IndexOf("AF");
            int fin = sipResponse.IndexOf("|AY");
            estado = sipResponse.Substring(inicio + 2, fin - inicio - 2);

            if (String.IsNullOrEmpty(estado))
                return true;
            else
                return false;
        }

        public InformacionTerminal respuesta_terminal(string sipResponse)
        {
            InformacionTerminal info_terminal = new InformacionTerminal();

            string estado_servidor = sipResponse.Substring(2, 1); //Estado del servidor     Y- N
            string estado_checkin = sipResponse.Substring(3, 1);
            string estado_checkout = sipResponse.Substring(4, 1);

            int posInicio = sipResponse.IndexOf("AO");
            int posPipe = sipResponse.IndexOf("|");
            string institucion = sipResponse.Substring(posInicio + 2, posPipe - posInicio - 2); //Institucion donde esta la estación de autoprestamo

            sipResponse = sipResponse.Substring(posPipe + 1);
            posInicio = sipResponse.IndexOf("AM");
            posPipe = sipResponse.IndexOf("|");
            string biblioteca = sipResponse.Substring(posInicio + 2, posPipe - posInicio - 2); //Biblioteca donde esta la estación de autoprestamo

            sipResponse = sipResponse.Substring(posPipe + 1);
            posInicio = sipResponse.IndexOf("AN");
            sipResponse = sipResponse.Substring(posInicio);

            posInicio = sipResponse.IndexOf("AN");
            posPipe = sipResponse.IndexOf("|");
            string localizacion = sipResponse.Substring(posInicio + 2, posPipe - posInicio - 2); //Ubicación de la terminal           

            //sipResponse = sipResponse.Substring(posPipe + 1);
            //posInicio = sipResponse.IndexOf("AF");
            //posPipe = sipResponse.IndexOf("|");
            //string estado = sipResponse.Substring(posInicio + 2, posPipe - posInicio - 2); //Ubicación de la terminal   

            info_terminal.institucion = institucion;
            info_terminal.biblioteca = biblioteca;
            info_terminal.localizacion = localizacion;
            info_terminal.estado_servidor = estado_servidor;
            info_terminal.estado_checkin = estado_checkin;
            info_terminal.estado_checkout = estado_checkout;
            //info_terminal.estado = estado;
            return info_terminal;
        }

        public InformacionUsuario respuesta_usuario(string sipResponse)
        {
            InformacionUsuario info_usuario = new InformacionUsuario();

            int posInicio = sipResponse.IndexOf("AO");
            int posPipe = sipResponse.IndexOf("|AA");
            string institucion = sipResponse.Substring(posInicio + 2, posPipe - posInicio - 2); //Institucion donde esta la estación de autoprestamo
            sipResponse = sipResponse.Substring(posPipe + 1);

            posInicio = sipResponse.IndexOf("AE");
            posPipe = sipResponse.IndexOf("|BZ");
            string nombreUsuario = sipResponse.Substring(posInicio + 2, posPipe - posInicio - 2); //Nombre del Usuario
            sipResponse = sipResponse.Substring(posPipe + 1);

            posInicio = sipResponse.IndexOf("CB");
            posPipe = sipResponse.IndexOf("|BL");
            string maxNumPrestamos = sipResponse.Substring(posInicio + 2, posPipe - posInicio - 2); //Numero de prestamos
            sipResponse = sipResponse.Substring(posPipe + 1);

            posInicio = sipResponse.IndexOf("BL");
            posPipe = sipResponse.IndexOf("|CQ");
            string usuarioValido = sipResponse.Substring(posInicio + 2, posPipe - posInicio - 2); //Validacion del Usuario
            sipResponse = sipResponse.Substring(posPipe + 1);

            posInicio = sipResponse.IndexOf("CQ");
            posPipe = sipResponse.IndexOf("|BH");
            string contrasenaValida = sipResponse.Substring(posInicio + 2, posPipe - posInicio - 2); //Validacion de la contraseña
            sipResponse = sipResponse.Substring(posPipe + 1);

            posInicio = sipResponse.IndexOf("BV");
            posPipe = sipResponse.IndexOf("|CC");
            string multa = sipResponse.Substring(posInicio + 2, posPipe - posInicio - 2); //Multa del Usuario
            sipResponse = sipResponse.Substring(posPipe + 1);

            string[] librosPrestados;
            //libros
            try
            {
                posInicio = sipResponse.IndexOf("AU");
                posPipe = sipResponse.IndexOf("|BD");
                if (posInicio != -1)
                {
                    string stringConLibros = sipResponse.Substring(posInicio + 2, posPipe - posInicio - 2); //Libros del Usuario
                    sipResponse = sipResponse.Substring(posPipe + 1);
                    librosPrestados = stringConLibros.Split('|');

                }
                else { librosPrestados = new string[0]; }

            }
            catch (Exception exc)
            {
                librosPrestados = new string[0];
            }

            posInicio = sipResponse.IndexOf("BE");
            posPipe = sipResponse.IndexOf("|BF");
            string correo = sipResponse.Substring(posInicio + 2, posPipe - posInicio - 2); //Mensaje opcional que puede traer informacion adicional
            sipResponse = sipResponse.Substring(posPipe + 1);

            posInicio = sipResponse.IndexOf("BF");
            posPipe = sipResponse.IndexOf("|AF");
            string telefono = sipResponse.Substring(posInicio + 2, posPipe - posInicio - 2); //Mensaje opcional que puede traer informacion adicional
            sipResponse = sipResponse.Substring(posPipe + 1);

            posInicio = sipResponse.IndexOf("AF");
            posPipe = sipResponse.IndexOf("|AY");
            string estado = sipResponse.Substring(posInicio + 2, posPipe - posInicio - 2); //Mensaje opcional que puede traer informacion adicional
            sipResponse = sipResponse.Substring(posPipe + 1);

            info_usuario.institucion = institucion;
            info_usuario.nombreUsuario = nombreUsuario;
            info_usuario.telefono = telefono;
            info_usuario.correo = correo;
            info_usuario.maxNumPrestamos = maxNumPrestamos;
            info_usuario.multa = multa;
            info_usuario.estado = estado;
            info_usuario.libros_prestados = librosPrestados;
            info_usuario.usuarioValido = usuarioValido;
            info_usuario.contrasenaValida = contrasenaValida;

            return info_usuario;
        }

        public InformacionEjemplar respuesta_ejemplar(string sipResponse)
        {
            InformacionEjemplar info_ejemplar = new InformacionEjemplar();

            string estado_ejemplar = sipResponse.Substring(2, 2); //Estado del servidor     Y- N

            switch (estado_ejemplar)
            {
                case "1":
                    estado_ejemplar = "other";
                    break;
                case "2":
                    estado_ejemplar = "on order";
                    break;
                case "3":
                    estado_ejemplar = "available";
                    break;
                case "4":
                    estado_ejemplar = "charged";
                    break;
                case "5":
                    estado_ejemplar = "charged; no to be recalled until earliest recall date";
                    break;
                case "6":
                    estado_ejemplar= "in process";
                    break;
                case "7":
                    estado_ejemplar= "recalled";
                    break;
                case "8":
                    estado_ejemplar= "waiting on hold shelf";
                    break;
                case "9":
                    estado_ejemplar = "waiting to be re-shield";
                    break;

            }


            int posInicio = sipResponse.IndexOf("CJ");
            int posPipe = sipResponse.IndexOf("|CM");
            string recall_date = sipResponse.Substring(posInicio + 2, posPipe - posInicio - 2);
            sipResponse = sipResponse.Substring(posPipe + 1);

            posInicio = sipResponse.IndexOf("CM");
            posPipe = sipResponse.IndexOf("|AB");
            string hold_pickup_date = sipResponse.Substring(posInicio + 2, posPipe - posInicio - 2);
            sipResponse = sipResponse.Substring(posPipe + 1);

            posInicio = sipResponse.IndexOf("AJ");
            posPipe = sipResponse.IndexOf("|BG");
            string titulo = sipResponse.Substring(posInicio + 2, posPipe - posInicio - 2); //Institucion donde esta la estación de autoprestamo
            string[] arrayTitulo = titulo.Split('/');
            sipResponse = sipResponse.Substring(posPipe + 1);

            info_ejemplar.fechaPrestamo = recall_date;
            info_ejemplar.fechaVencimiento = hold_pickup_date;
            info_ejemplar.titulo = arrayTitulo;

            return info_ejemplar;
        }




    }
}
