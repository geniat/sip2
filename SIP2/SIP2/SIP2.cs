﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;


namespace SIP2
{
    public class SIP2 
    {
        Respuestas respuesta = new Respuestas();

        string integra = ConfigurationManager.AppSettings["integra"];

        //Conexion SIP
        SIP2CON.SipConnection conexion;
        public string puerto = "6000";
        public string terminal = "8371100679279872";
        public string host = "udca.janium.net";

        
        public SIP2()
        {
            switch (integra)
            {
                case "SIP2":
                    conexion = new SIP2CON.SipConnection(host, puerto, terminal, terminal);
                    break;
                case "JANIUM":
                    break;
                case "SIABUC":
                    break;
                case "KOHA":
                    break;
            }
        }

        //Metodo con información del usuario- No contiene infomación de los libros prestados.
        public InformacionUsuario PatronStatusRequest(string institucionID, string usuario, string terminalPassword, string usuarioPassword)
        {
            conexion.Open();
            string date = GetDateString();
            string mensaje = string.Format("23{0}AO{1}|AA{2}|AC{3}|AD{4}|AY3AZ", date, institucionID, usuario, terminalPassword, usuarioPassword);
            string checksum = CalculateChecksum(mensaje);
            mensaje += checksum.ToUpper();
            string sipCommand = string.Format(mensaje);
            string sipResponse = conexion.SipFactory(sipCommand); // Metodo para pedir la informacion//SipFactory(sipCommand);         ////////CAMBIAR
            return respuesta.respuesta_PatronStatusResponse(sipResponse);
            conexion.Dispose();
        }

        // Metodo para el autoprestamo de libro:
        public bool CheckOut(string institucionID, string usuario, string codigoBarra, string terminalPassword, string usuarioPassword)
        {
            string date = GetDateString();
            string mensaje = string.Format("11NN{0}                  AO{1}|AA{2}|AB{3}|AC{4}|AD{5}|BON|BIN|AY3AZ", date, institucionID, usuario, codigoBarra, terminalPassword, usuarioPassword);
            string checksum = CalculateChecksum(mensaje);
            mensaje += checksum.ToUpper();
            string sipCommand = string.Format(mensaje);
            string sipResponse = null;//Metodo que procesa la solicitud //SipFactory(sipCommand);        ////////CAMBIAR
            return respuesta.respuesta_CheckOut(sipResponse);
        }

        // Metodo para la autodevolucion de libro
        public bool CheckIn(string localizacion, string institucionID, string codigoBarra, string terminalPassword)
        {
            conexion.Open();
            string date = GetDateString();
            string mensaje = string.Format("09N{0}{0}AP{1}|AO{2}|AB{3}|AC{4}|BIN|AY3AZ", date, localizacion, institucionID, codigoBarra, terminalPassword);
            string checksum = CalculateChecksum(mensaje);
            mensaje += checksum.ToUpper();
            string sipCommand = string.Format(mensaje);
            string sipResponse = conexion.SipFactory(sipCommand); //Metodo que procesa el comando//SipFactory(sipCommand);         ////////CAMBIAR
            return respuesta.respuesta_CheckIn(sipResponse);
            conexion.Dispose();
        }

        //++++++++++++No se aun que devuelve buscar que es AL blocked card msg = xxxxx+++++++++///////
        public void BlockPatron(string institucionID, string xxxxx, string usuario, string terminalPassword)
        {
            string date = GetDateString();
            string mensaje = string.Format("01{0}AO{1}|AL{2}|AA{3}|AC{4}|AY3AZ", date, institucionID, xxxxx, usuario, terminalPassword);
            string checksum = CalculateChecksum(mensaje);
            mensaje += checksum.ToUpper();
            string sipCommand = string.Format(mensaje);
            string sipResponse = null; //Metodo que procesa el comando//SipFactory(sipCommand);         ////////CAMBIAR

        }

        // Metodo para verificar el estado del Servidor
        public InformacionTerminal SCStatus()
        {
            conexion.Open();
            string mensaje = "9900002.00AY5AZ";
            string checksum = CalculateChecksum(mensaje);
            mensaje += checksum.ToUpper();
            string sipCommand = string.Format(mensaje);
            string sipResponse = conexion.SipFactory(sipCommand); // Metodo para pedir la informacion //SipFactory(sipCommand);    /////////CAMBIAR
            return respuesta.respuesta_terminal(sipResponse);
            conexion.Dispose();
        }

        public InformacionUsuario PatronInformation(string institucionID, string usuario, string terminalPassword, string usuarioPassword)//string patronBarcode, IEnumerable<string> itemBarcodes)
        {
            string date = GetDateString();
            string mensaje = string.Format("63000{0}  Y       AO{1}|AA{2}|AC{3}|AD{4}|AY3AZ", date, institucionID, usuario, terminalPassword, usuarioPassword);
            string checksum = CalculateChecksum(mensaje);
            mensaje += checksum.ToUpper();
            string sipCommand = string.Format(mensaje);
            string sipResponse = null; // Metodo para pedir la informacion//SipFactory(sipCommand);         ////////CAMBIAR
            return respuesta.respuesta_usuario(sipResponse);
        }


        public void FeePaid(string institucionID, string usuario, string terminalPassword, string usuarioPassword)
        {
            string date = GetDateString();
            string mensaje = string.Format("37{0}AO{1}|AA{2}|AC{3}|AD{4}|AY3AZ", date, institucionID, usuario, terminalPassword, usuarioPassword);
            string checksum = CalculateChecksum(mensaje);
            mensaje += checksum.ToUpper();
            string sipCommand = string.Format(mensaje);
            string sipResponse = null; // Metodo para pedir la informacion//SipFactory(sipCommand);         ////////CAMBIAR
        }

        public InformacionEjemplar ItemInformation(string institucionID, string codigoBarra, string terminalPassword)//string patronBarcode, IEnumerable<string> itemBarcodes)
        {
            string date = GetDateString();
            string mensaje = string.Format("17{0}AO{1}|AB{2}|AC{3}|AY3AZ", date, institucionID, codigoBarra, terminalPassword);
            string checksum = CalculateChecksum(mensaje);
            mensaje += checksum.ToUpper();
            string sipCommand = string.Format(mensaje);
            string sipResponse = null;// Metodo para pedir la informacion//SipFactory(sipCommand);         ////////CAMBIAR
            return respuesta.respuesta_ejemplar(sipResponse);
        }

        //Actualizacion de Item sin tener que hacer Checkout o Checkin
        public void ItemStatusUpdate(string institucionID, string codigoBarra, string terminalPassword)
        {
            string date = GetDateString();
            string mensaje = string.Format("19{0}AO{1}|AB{2}|AC{3}|AY3AZ", date, institucionID, codigoBarra, terminalPassword);
            string checksum = CalculateChecksum(mensaje);
            mensaje += checksum.ToUpper();
            string sipCommand = string.Format(mensaje);
            string sipResponse = null; // Metodo para pedir la informacion//SipFactory(sipCommand);         ////////CAMBIAR
        }


        /////////////////////////////////////////////////////////
        // Metodo para calcular el Checksum
        private string CalculateChecksum(string mensaje)
        {
            char[] charArray = mensaje.ToCharArray();
            int suma = 0;
            foreach (char c in charArray)
            {
                suma = suma + c;
            }
            string binary = Convert.ToString(suma, 2);

            while (binary.Length != 16)
            {
                binary = "0" + binary;
            }
            string binarioInvertido = "";
            char[] arrayBinario = binary.ToCharArray();
            for (int i = 0; i < binary.Length; i++)
            {
                char c = arrayBinario[i];
                if (c == '1')
                {
                    binarioInvertido += "0";
                }
                else
                {
                    binarioInvertido += "1";
                }

            }
            int valorIntero = Convert.ToInt32(binarioInvertido, 2);
            valorIntero++;
            string respuestaHexadecimal = Convert.ToString(valorIntero, 16);
            return respuestaHexadecimal;
        }

        /////////////////////////////////////////////////////////
        // Metodo Fecha de la transaccion
        private string GetDateString()
        {
            string year = DateTime.Now.Year.ToString("00");
            string month = DateTime.Now.Month.ToString("00");
            string day = DateTime.Now.Day.ToString("00");
            string ZZZZ = "    ";
            string hour = DateTime.Now.Hour.ToString("00");
            string minute = DateTime.Now.Minute.ToString("00");
            string second = DateTime.Now.Second.ToString("00");
            return year + month + day + ZZZZ + hour + minute + second;
        }
    }
}
