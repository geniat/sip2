﻿using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Collections;
using System.Collections.Generic;

namespace SIP2CON
{
    public class SipConnection : IDisposable
    {

        /*********************************************
        * Instance Variables
        *********************************************/

        private TcpClient sipSocket = new TcpClient();
        private NetworkStream sipStream = null;
        private SipServerParameters sip = new SipServerParameters();
        private bool connected = false;
        private bool authorized = false;
        private int contador = 0;


        /*********************************************
        * Constructors
        *********************************************/
        public SipConnection() { }

        // <summary>
        ///     SIP Constructor taking the connection information as string parameters.
        /// </summary>
        /// <param name="ip">SIP server IP</param>
        /// <param name="port">SIP server port</param>
        /// <param name="username">SIP server username</param>
        /// <param name="password">SIP server password</param>
        /// <param name="extra_number">SIP server extra number (optional in some implementations)</param>
        public SipConnection(string ip, string port, string username, string password, string extra_number = "")
        {
            this.sip.ip = ip;
            this.sip.port = port;
            this.sip.username = username;
            this.sip.password = password;
            this.sip.extra_number = extra_number;
        }

        /// <summary>
        ///     SIP constructor taking an instance of the SipServerParameter class as it's parameter
        /// </summary>
        /// <param name="sipParameters">Instance of the SipServerParameter class</param>
        /// <remarks>
        ///     Use this constructor if you will be regularly accessing and/or changing your SIP server parameters in your application.
        /// </remarks>
        public SipConnection(SipServerParameters sipParameters)
        {
            this.sip.ip = sipParameters.ip;
            this.sip.port = sipParameters.port;
            this.sip.username = sipParameters.username;
            this.sip.password = sipParameters.password;
            this.sip.extra_number = sipParameters.extra_number;
        }

        /*********************************************
        * Public Methods
        *********************************************/

        /// <summary>
        ///     Starts SIP connection assuming that SIP parameters were defined in the contructor.
        /// </summary>
        public bool Open()
        {
            bool resp = false;
            //  Set up socket.
            sipSocket = new TcpClient();
            sipSocket.SendTimeout = 500;
            try
            {
                sipSocket.Connect(sip.ip, Convert.ToInt32(sip.port));
                sipStream = sipSocket.GetStream();
                resp = true;
            }
            catch { }

            return resp;
        }

        /// <summary>
        ///     Starts connection with SIP server.  Parameters are required.  Use this if you do not establish server parameteres in the constructor.
        /// </summary>
        /// <param name="ip">SIP server IP</param>
        /// <param name="port">SIP server port</param>
        /// <param name="username">SIP server username</param>
        /// <param name="password">SIP server password</param>
        /// <param name="extra_number">SIP server extra number (optional in some implementations)</param>
        public void Open(string ip, string port, string username, string password, string extra_number)
        {
            //  Set up instance variables.   
            this.sip.ip = ip;
            this.sip.port = port;
            this.sip.username = username;
            this.sip.password = password;
            this.sip.extra_number = extra_number;
            Open();
        }

        //Closes SIP connection.
        public void Close()
        {
            if ((sipStream != null) & (sipSocket != null))
            {
                sipStream.Close();
                sipSocket.Close();
                sipStream = null;
                sipSocket = null;
            }
            else throw new NotConnectedException("Cannot close connection.  Connection was not established!");
        }

        //   
        // Metodo escritura y lectura en el puerto TCP/IP.
        public string SipFactory(string sipCommand)
        {
            try
            {
                //  Set up and submit outstream data.
                byte[] outStream = Encoding.ASCII.GetBytes(sipCommand + "\r");
                sipStream.Write(outStream, 0, outStream.Length);
                sipStream.Flush();

                //  Read SIP socket response.
                byte[] inStream = new byte[100025];  //  I have no idea why you need this many bytes.
                sipStream.Read(inStream, 0, (int)sipSocket.ReceiveBufferSize);
                string sipResponse = Encoding.ASCII.GetString(inStream).Trim();
                return sipResponse;
            }
            catch (Exception ex)
            {
                return ex.ToString();  //  For debugging purposes or if the socket fails for some reason.  
            }
        }

        public void Dispose()
        {
            Close();
        }

    }
}
