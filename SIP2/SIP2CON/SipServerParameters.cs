﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace SIP2CON
{
    public class SipServerParameters
    {
        public string ip;
        public string port;
        public string username;
        public string password;
        public string extra_number;
        public string terminal_password;

    }
}
